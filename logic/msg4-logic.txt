//only 30 characters out of 50 are used
//msg4 length 635
//{'D': 105, 'L': 63, 'P': 45, 'N': 42, '1': 40, 'K': 38, '(': 35, '.': 30, ':': 27, 'J': 23, ',': 20, '8': 20, 'R': 17, 'I': 16, 'Y': 16, 'T': 14, ' ': 13, 'O': 13, '?': 9, '\n': 8, '3': 8, 'X': 7, 'Q': 6, '2': 5, '-': 4, 'G': 4, '$': 3, 'S': 2, '!': 1, 'M': 1}
//characters used in string (sorted least to most usage): M!S$G-2QX\n3?O TIYR,8J:.(K1NPLD
//possible plain characters:ABCDEFGHIJKLMNOPQRSTUVWXYZ .,\n .NOTE: \n quite unsured used or not.
// https://en.oxforddictionaries.com/explore/which-letters-are-used-most
//most frequence used characters: whitespce, e, a

whitespace
E
A
N
O
I
T?G => T
F
S
R
M
V
U
Y
D
G
H
...

Count char in cipher => only 30 char
Educated guess that 30 char used in original alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ .,\n"
Create temp cipher string according to frequency in ascending order = "M!S$G-2QX\n3?O TIYR,8J:.(K1NPLD"
Create temp alphabet according to frequency in ascending order, but put \n at the end just because
"\n,.QJZXVKWYFBGHMPDUCLSNTOIRAE "
assume whitespace, E mapped to the 2 most frequent used: D, L
find word of 1 char => find A
find words of 2 char
    Since A is good => possible words: AS, AT, AN
    Other word end with S => US
    Other word end with T => IT
    Other word end with N => IN, ON
    find N
    find O
    find I
    Word end with O => TO, SO, DO, GO, NO

TODO
Automate the indexing and mapping process

NOTE: 2 characters in plain haven't determined are Q and Z
NOTE: 22 Jul Changed Q and Z in plain key to \n and - respectively
NOTE: THE is the most used words in english
