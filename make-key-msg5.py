with open("dcrypted_msg4.txt", "r") as f:
    dmsg4 = f.read()


I_indexes = [i for i, ltr in enumerate(dmsg4) if ltr == 'I']
key_length = 300


def get_index(fixed_position, distance_index, message_length):
    """
    :param fixed_position: int
    :param distance_index: int
    :param message_length: int
    :return: int
    """
    return message_length + (fixed_position - distance_index) \
        if (fixed_position - distance_index) < 0 \
        else fixed_position - distance_index

result_keys = []
for i_index in I_indexes:
    key = []
    for i in reversed(xrange(key_length)):  # get 300 characters before every I character in msg4
        index = get_index(i_index, i, len(dmsg4))
        key.append(dmsg4[index])
    result_keys.append(''.join(key))


with open("msg5-keys-trial.txt", "w+") as f:
    for key in result_keys:
        f.write(key)
        f.write('===')
