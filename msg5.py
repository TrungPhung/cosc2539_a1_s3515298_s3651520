
def decrypted_vernam_char(cipher_char, key_char, alphb):
    plain_index = alphb.index(cipher_char) - alphb.index(key_char)
    if plain_index < 0:
        result_index = plain_index + len(alphb)
    else:
        result_index = plain_index
    return alphb[result_index]


def decrypt_vernam(cipher_text, key, alphabet):
    result = []
    for i, char in enumerate(cipher_text):
        result.append(decrypted_vernam_char(char, key[i], alphabet))
    return ''.join(result)

with open("COSC2539_A1_msgs_2017B/msg5.enc", "r") as f:
    msg5 = f.read()
    msg5 = msg5[1:(len(msg5) - 2)]

with open("alphabet.txt", "r") as f:
    alphabet = f.read().replace("\\n", "\n")  # correct the "return" char

with open("msg5-key.txt", "r") as f:
    key = f.read()

with open("result/dcrypted_msg5.txt", "w+") as f:
    f.write("key = >" + key + "<\n\n")
    f.write("Decrypted Message:\n\n>" + decrypt_vernam(msg5, key, alphabet) + "<")
