def random_sub_decrypt(plain, cipher, message):
    """
       Args:
            plain (str): Plain string
            cipher (str): Cipher string
            message (str): The encrypted message
    """
    decrypted_msg = []
    for char in message:
        for j, cipher_char in enumerate(cipher):
            if char == cipher_char:
                decrypted_msg.append(plain[j])
    return ''.join(decrypted_msg)

alphabet = "\n,.QJZXVKWYFBGHMPDUCLSNTOIRAE "
#test_alphabet = "JABCNEFGHI\nKLMDOPQRSTUVWXYZ., "
alphabet2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ .,'"
alphabet3 = "ABCDEFGHIJKLMNOP-RSTUVWXY\n .,'"
msg4 = ''

# with open("alphabet.txt", "r") as f:
#     alphabet = f.read().replace("\\n", "\n")  # correct the "return" char

with open("COSC2539_A1_msgs_2017B/msg4.enc", "r") as f:
    msg4 = f.read()
    msg4 = msg4[1:(len(msg4) - 1)]

cipher_string = "(Q IL?RYK!GJONPTS.:1,\n3M8-DX2$"
cipher_string_bak = "M!S$G-2QX\n3?O TIYR,8J:.(K1NPLD"

result = random_sub_decrypt(alphabet, cipher_string_bak, msg4)
print result
print "\n"
print filter(lambda x: len(x) == 2, result.split(" "))
print filter(lambda x: len(x) == 4, result.split(" "))
with open("result/dcrypted_msg4.txt", "w+") as f:
    f.write(result)
