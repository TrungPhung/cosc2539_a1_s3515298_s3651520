def get_cipher(plain, cipher_key):
    """
    Args:
        plain (str): Plain string
        cipher_key (int): Cipher key
    """
    shifted_list = []
    for i, char in enumerate(plain):
        si = (i + cipher_key) % 50  # shifted index
        shifted_list.append(plain[si])
    return ''.join(shifted_list)


def ceasar_decrypt(plain, cipher, message):
    """
       Args:
            plain (str): Plain string
            cipher (str): Cipher string
            message (str): The encrypted message
    """
    decrypted_msg = []
    for char in message:
        for j, cipher_char in enumerate(cipher):
            if char == cipher_char:
                decrypted_msg.append(plain[j])
    return ''.join(decrypted_msg)

# alphabet = ''
# msg1 = ''
key = 11

with open("alphabet.txt", "r") as f:
    alphabet = f.read().replace("\\n", "\n")  # correct the "return" char

with open("COSC2539_A1_msgs_2017B/msg1.enc", "r") as f:
    msg1 = f.read()
    msg1 = msg1[1:(len(msg1) - 1)]

cipher_string = get_cipher(alphabet, key)
result = ceasar_decrypt(alphabet, cipher_string, msg1)
print "key = " +  str(key) + "\n"
print result
with open("result/dcrypted_msg1.txt", "w+") as f:
    f.write("key = " +  str(key) + "\n\n")
    f.write(result)
