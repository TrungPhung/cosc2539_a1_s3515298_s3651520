def find_factor(n):
    """

    :param n: an arbitrary integer
    :return: list of factors of n
    """
    factor_list = []
    for i in range(2, n - 1):
        if n % i == 0:
            factor_list.append(i)
    return factor_list


def get_ceasar_cipher(plain, cipher_key):
    """
    Args:
        plain (str): Plain string
        cipher_key (int): Cipher key
    """
    shifted_list = []
    for i, char in enumerate(plain):
        si = (i + cipher_key) % 50  # shifted index
        shifted_list.append(plain[si])
    return ''.join(shifted_list)


def decrypt_col(key, cipher):
    """

    :param key: integer
    :param cipher: string
    :return: string
    """
    column = key
    row = len(cipher) / key
    k = 0
    decrypt_table = []

    for i in range(column):
        decrypt_row = []
        for j in range(row):
            if k < len(cipher):
                decrypt_row.append(cipher[k])
                k = k + 1
        decrypt_table.append(decrypt_row)
    decrypted_msg = ""
    for i in range(row):
        for j in range(column):
            decrypted_msg += decrypt_table[j][i]
    return decrypted_msg


def ceasar_decrypt(plain, cipher, message):
    """
       Args:
            plain (str): Plain string
            cipher (str): Cipher string
            message (str): The encrypted message
    """
    decrypted_msg = []
    for char in message:
        for j, cipher_char in enumerate(cipher):
            if char == cipher_char:
                decrypted_msg.append(plain[j])
    return ''.join(decrypted_msg)

with open("alphabet.txt", "r") as f:
    alphabet = f.read().replace("\\n", "\n")  # correct the "return" char

with open("COSC2539_A1_msgs_2017B/msg6.enc2", "r") as f:
    msg6 = f.read()
    msg6 = msg6[1:(len(msg6) - 2)]

factor_list = find_factor(len(msg6))
key_list = filter(lambda x: x < 50, factor_list)
print len(msg6)
print key_list
result = {}
for key in key_list:
    cipher = get_ceasar_cipher(alphabet, key)
    decrypt_msg6_1 = ceasar_decrypt(alphabet, cipher, msg6)
    decrypted_msg6 = decrypt_col(key, decrypt_msg6_1)
    result[key] = decrypted_msg6

with open("result/dcrypted_msg6.txt", "w+") as f:
    for key, value in result.items():
        f.write("\n==============\n")
        f.write("\nkey = " + str(key) + "\n")
        f.write(value)
        f.write("\n==============\n")
