def random_sub_decrypt(plain, cipher, message):
    """
       Args:
            plain (str): Plain string
            cipher (str): Cipher string
            message (str): The encrypted message
    """
    decrypted_msg = []
    for char in message:
        for j, cipher_char in enumerate(cipher):
            if char == cipher_char:
                decrypted_msg.append(plain[j])
    return ''.join(decrypted_msg)

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ .,\n"
alphabet2 = "ABCDEFGHIXKLMNOPQRSTUVW'Y- .,\n"

with open("COSC2539_A1_msgs_2017B/msg7.enc", "r") as f:
    msg7 = f.read()
    msg7 = msg7[1:(len(msg7) - 1)]

cipher_string = "JD74TZ-K$?C0Q2\"9PAUHL1B6Y8F SN"
result = random_sub_decrypt(alphabet2, cipher_string, msg7)
# print result
# print "\n"
# wo2 = filter(lambda x: len(x) == 2, result.split(" "))
# print list(set(wo2))
# print list(set(filter(lambda x: len(x) == 3, result.split(" "))))
# print list(set(filter(lambda x: len(x) == 4, result.split(" "))))
# print filter(lambda x: len(x) > 4, list(set(result.split(" "))))
with open("result/dcrypted_msg7.txt", "w+") as f:
    f.write("key: " + alphabet2.replace("\n", "\\n") + "=" + cipher_string + "\n\n")
    f.write("Decrypted Message: \n\n>" + result + "<")
