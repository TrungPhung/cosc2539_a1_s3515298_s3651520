def decrypt_col(key, cipher):
    """

    :param key: integer
    :param cipher: string
    :return: string
    """
    column = key
    row = len(cipher) / key
    k = 0
    decrypt_table = []

    for i in range(column):
        decrypt_row = []
        for j in range(row):
            if k < len(cipher):
                decrypt_row.append(cipher[k])
                k = k + 1
        decrypt_table.append(decrypt_row)
    decrypted_msg = ""
    for i in range(row):
        for j in range(column):
            decrypted_msg += decrypt_table[j][i]
    return decrypted_msg

with open("alphabet.txt", "r") as f:
    alphabet = f.read().replace("\\n", "\n")  # correct the "return" char

with open("COSC2539_A1_msgs_2017B/msg3.enc", "r") as f:
    msg3 = f.read()
    msg3 = msg3[1:(len(msg3) - 2)]


key = 20
plain = decrypt_col(key, msg3)

print "key = " + str(key) + "\n"
print plain

with open("result/dcrypted_msg3.txt", "w+") as f:
    f.write("key = " + str(key) +"\n\n")
    f.write(plain)

