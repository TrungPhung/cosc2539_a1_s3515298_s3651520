listOf2 = ['ZW', 'WM', '.Y', '.V', 'JK', 'AZ', 'WY']
a_possibilities = ['N', 'T', 'S']
frequent_2letter_words = ['OF', 'TO', 'IN', 'IT', 'IS', 'BE', 'AS', 'AT', 'SO', 'WE', 'HE', 'BY', 'OR', 'ON', 'DO',
                          'IF', 'ME', 'MY', 'UP', 'AN', 'GO', 'NO', 'US', 'AM']


def replace_char(string, from_char, to_char):
    replaced_string = ''
    for char in string:
        if(char == from_char):
            replaced_string += to_char
        else:
            replaced_string += char
    return replaced_string


def replace_char_in_list(list, from_char, to_char):
    replace_list = []
    for e in list:
        replace_list.append(replace_char(e, from_char, to_char))
    return replace_list


def get_list_of_string_contain_char(list, char):
    pair_list = []
    for e in list:
        if char in e:
            pair_list.append((e.index(char), e))
    return pair_list


def get_relevant_char(finding_char, index):
    pos_char = []
    for word in frequent_2letter_words:
        if word[index] == finding_char:
            pos_char.append(word[(index + 1) % 2])
    return pos_char


for possibility in a_possibilities:
    existed_char = ['A']
    ensured_pair = []
    a_char = filter(lambda x: x[0] == 'A', listOf2)[0]
    test_char = a_char[1]
    existed_char.append(test_char)
    existed_char = list(set(existed_char))
    ensured_pair.append('A' + possibility)
    replaced_list = replace_char_in_list(listOf2, test_char, possibility)
    for i, value in get_list_of_string_contain_char(replaced_list, possibility):
        for j in get_relevant_char(possibility, i):
            replaced_list_1 = replace_char_in_list(replaced_list, value, j)
            # print replaced_list_1


print get_relevant_char('I', 0)
