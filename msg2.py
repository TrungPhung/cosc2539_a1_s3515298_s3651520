def find_factor(n):
    """

    :param n: an arbitrary integer
    :return: list of factors of n
    """
    factor_list = []
    for i in range(2, n - 1):
        if n % i == 0:
            factor_list.append(i)
    return factor_list


def decrypt_col(key, cipher):
    """

    :param key: integer
    :param cipher: string
    :return: string
    """
    column = key
    row = len(cipher) / key
    k = 0
    decrypt_table = []

    for i in range(column):
        decrypt_row = []
        for j in range(row):
            if k < len(cipher):
                decrypt_row.append(cipher[k])
                k = k + 1
        decrypt_table.append(decrypt_row)
    decrypted_msg = ""
    for i in range(row):
        for j in range(column):
            decrypted_msg += decrypt_table[j][i]
    return decrypted_msg

#alternative way
def decrypt_col_2(key, cipher):
    plain = []
    column = key
    row = int(len(cipher) / key)
    for i in range(row):
        index = i
        for j in range(column):
            plain.append(cipher[index])
            index += row
    return ''.join(plain)

with open("alphabet.txt", "r") as f:
    alphabet = f.read().replace("\\n", "\n")  # correct the "return" char

with open("COSC2539_A1_msgs_2017B/msg2.enc", "r") as f:
    msg2 = f.read()
    msg2 = msg2[1:(len(msg2) - 2)]

factor_list = find_factor(len(msg2))

"""
brute force steps to find the key
"""
# for i in range(len(factor_list)):
#     key = factor_list[i]
#     print "\nkey = " + str(key)
#     plain = decrypt_col(key, msg2)
#     print plain
#     print "\n=============================="

key = 18
plain = decrypt_col(key, msg2)

print "key = " + str(key) + "\n"
print plain

with open("result/dcrypted_msg2.txt", "w+") as f:
    f.write("key = " + str(key) +"\n\n")
    f.write(plain)

